import numpy as np
from time import sleep
from os import system

class GameOfLife:

    def __init__(self, initial_grid, time_unit):
        """Description: Constructor of a class.
        Parameters:
            initial_grid: 2D numpy array
                Each elements of the array is eather Dead 0 or alive 1.
            time_unit: Integer
                The amount of time in seconds between each generation.
        Return:
            Instance of the class.

        """
        self.grid = initial_grid
        self.time_unit = time_unit
    
    def count_alive_neighbours(self, cell_coordinate):
        """Description: count_alive_neighbours compute number of neighbours alive
        Parameters:
            cell_coordinate: tuple or list
                            (row, col) position.
        Return:
            Integer numbers of neighbours alive.

        """
        n_rows, n_cols = self.grid.shape

        if cell_coordinate[0] == (n_rows - 1) and cell_coordinate[1] == (n_cols - 1): # check bottom right corner
        
            i_start, i_end = cell_coordinate[0] - 1, cell_coordinate[0] + 1
            j_start, j_end = cell_coordinate[1] - 1, cell_coordinate[1] + 1
            i, j = 1, 1

        elif cell_coordinate[0] == 0 and cell_coordinate[1] == 0: # check top left corner
        
            i_start, i_end = cell_coordinate[0], cell_coordinate[0] + 2
            j_start, j_end = cell_coordinate[1], cell_coordinate[1] + 2
            i, j = 0, 0
        
        elif cell_coordinate[0] == (n_rows - 1) and cell_coordinate[1] == 0: # check botto left corner

            i_start, i_end = cell_coordinate[0] - 1, cell_coordinate[0] + 1
            j_start, j_end = cell_coordinate[1], cell_coordinate[1] + 2
            i, j = 1, 0
        
        elif cell_coordinate[0] == 0 and cell_coordinate[1] == (n_cols - 1): # check top right corner

            i_start, i_end = cell_coordinate[0], cell_coordinate[0] + 2
            j_start, j_end = cell_coordinate[1] - 1, cell_coordinate[1] + 1
            i, j = 0, 1

        elif cell_coordinate[0] == 0 and 0 < cell_coordinate[1] < (n_cols - 1): # check top border

            i_start, i_end = cell_coordinate[0], cell_coordinate[0] + 2
            j_start, j_end = cell_coordinate[1] - 1, cell_coordinate[1] + 2
            i, j = 0, 1

        elif cell_coordinate[0] == (n_rows - 1) and 0 < cell_coordinate[0] < (n_rows - 1): #  bottom border

            i_start, i_end = cell_coordinate[0] - 1, cell_coordinate[0] + 1
            j_start, j_end = cell_coordinate[1] - 1, cell_coordinate[1] + 2
            i ,j = 1, 1
        
        elif cell_coordinate[1] == 0 and 0 < cell_coordinate[0] < (n_rows - 1): # check left border
        
            i_start, i_end = cell_coordinate[0] - 1, cell_coordinate[0] + 2
            j_start, j_end = cell_coordinate[1], cell_coordinate[1] + 2
            i, j = 1, 0
        
        elif cell_coordinate[0] == (n_cols - 1) and 0 < cell_coordinate[0] < (n_rows - 1): # check right border
        
            i_start, i_end = cell_coordinate[0] - 1, cell_coordinate[0] + 2
            j_start, j_end = cell_coordinate[1] - 1, cell_coordinate[1] + 1
            i ,j = 1, 1

        else:
        
            i_start, i_end = cell_coordinate[0] - 1, cell_coordinate[0] + 2
            j_start, j_end = cell_coordinate[1] - 1, cell_coordinate[1] + 2
            i, j = 1, 1
        
        neighbourhood = self.grid[i_start:i_end, j_start:j_end]

        return neighbourhood.sum() - neighbourhood[i, j]

    def compute_next_gen(self):
        """Description: instance methode compute_next_gen : compute grid next generation.
        Parameter:

        Return:        
        """
        next_grid = np.zeros(self.grid.shape, dtype="int")

        n_rows, n_cols = self.grid.shape
        for i in range(n_rows):
            for j in range(n_cols):
                ### check rules
                n_neighbours = self.count_alive_neighbours(cell_coordinate=(i, j))

                if self.grid[i, j] == 1 and np.isin(n_neighbours, np.arange(2, 4)):
                    next_grid[i, j] = 1
                elif self.grid[i, j] == 0 and n_neighbours == 3:
                    next_grid[i, j] = 1

        self.grid = next_grid
    
    def print_grid(self):
        n_rows, n_cols = self.grid.shape

        line_sep = " " + "_______ "*n_cols
        inter_linge = "|" + "       |"*n_cols
        for i in range(n_rows):
            print(line_sep)
            print(inter_linge)
            print("|", end="")
            for j in range(n_cols):
                print(f"   {self.grid[i, j]}   |", end="")
            print("")
        print(line_sep)

    def run(self, n_gen):
        command = 'clear'
        for i in range(n_gen):
            system(command)
            self.print_grid()
            sleep(self.time_unit)
            self.compute_next_gen()
            if self.grid.max() == 0: break
